// Отримуємо посилання на елементи форми
const priceInput = document.getElementById('price');

// Функція для валідації введеного значення ціни
function validatePrice() {
  const priceValue = parseFloat(priceInput.value);
  if (priceValue < 0 || isNaN(priceValue)) {
    // Якщо введене число менше 0 або не є числом, то підсвічуємо поле червоним і виводимо під ним повідомлення
    priceInput.classList.add('invalid');
    priceInput.classList.remove('valid');
    priceInput.removeAttribute('value');
    const errorMessage = document.createElement('span');
    errorMessage.classList.add('error-message');
    errorMessage.textContent = 'Please enter correct price.';
    priceInput.parentNode.insertBefore(errorMessage, priceInput.nextSibling);
  } else {
    // Інакше підсвічуємо поле зеленим і створюємо span з текстом і кнопкою X
    priceInput.classList.add('valid');
    priceInput.classList.remove('invalid');
    const priceSpan = document.createElement('span');
    priceSpan.classList.add('price-span');
    priceSpan.textContent = `Current price: $${priceValue}`;
    const closeButton = document.createElement('button');
    closeButton.classList.add('close-button');
    closeButton.innerHTML = '&times;';
    closeButton.addEventListener('click', function() {
      priceSpan.remove();
      priceInput.value = '';
      priceInput.classList.remove('valid');
      priceInput.classList.remove('invalid');
    });
    priceSpan.appendChild(closeButton);
    priceInput.parentNode.insertBefore(priceSpan, priceInput.nextSibling);
  }
}

// Додаємо обробник події для валідації при втраті фокусу з поля вводу
priceInput.addEventListener('blur', validatePrice);

// Додаємо обробник події для зняття підсвічування при фокусуванні на полі вводу
priceInput.addEventListener('focus', function() {
  priceInput.classList.remove('valid');
  priceInput.classList.remove('invalid');
  const errorMessage = document.querySelector('.error-message');
  if (errorMessage) {
    errorMessage.remove();
  }
});